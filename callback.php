<?php
	require_once('config.php'); 
	require_once('fsq_api.php');
	require_once('storify_db.php');

	$db	= new StorifyDb();
	$fsq	= new FourSquare();

	parse_str($_SERVER['QUERY_STRING'], $query);
	$code = $query['code'];

	$access_token = $fsq->get_access_token($code);

	$expire = time()+2592000; 
	setcookie("foursquare_token", $access_token, $expire, '/');

	$user = $fsq->get_user($access_token, 'self');
	#$db->create_user($user, 1);
	#echo json_encode($user);

	$checkins = $fsq->get_checkins($access_token);
	/*foreach ($checkins as $checkin){
		#$db->create_checkin($checkin, $user['fq_id']);
		echo json_encode($checkin['like']);

	}*/

	#echo json_encode($checkins->like);
	
	$outer = Array();
	$out = Array();
	$out['headline'] 	= $user['name']."'s timeline";
	$out['type']		= "default";
	$out['text']		= '<div class=\'hero-unit\'>I have <b>'.$user['ccount'].'</b> Checkins and <b>'.$user['fcount'].'</b> Friends</div>';
	$out['startDate']	= '';#date('Y,m,d',$checkin['createdAt']);
	$outer_asset = Array();
	$outer_asset['media']   = $user['photo'];
	$outer_asset['caption']	= '';
	$outer_asset['credit']	= '';
	$out['asset']		= $outer_asset;
	$dates = Array();
	$i = 0;
	foreach($checkins as $checkin){
		$date			= Array();
		$date['startDate'] 	= date('Y,m,d',$checkin['time']);
		$date['headline']	= $checkin['venue']['name'];
		#$date['headline']	= $user['name']. " was @ ".$checkin['venue']['name'];
		$data_text		= "<div class='hero-unit'><em><b>".$checkin['shout']."</b></em><div class='profile-block'><span class='badge'>2 </span> Friends have been here <br><img class='friends-image img-circle' src='https://is1.4sqi.net/userpix_thumbs/AXVDINXEALS413JJ.jpg'/><img class='friends-image-with-margin img-circle' src='https://is1.4sqi.net/userpix_thumbs/5KFUXMTBEKBWWU22.jpg'/></div><div class='profile-block'>You have checked in 2 times here</div>";
		if(count($checkin['like']) > 0){
			$data_text	= $data_text."<div class='profile-block'><span class='badge'>".count($checkin['like'])."</span> Friends have liked it<br>";
			foreach($checkin['like'] as $like_user)
				$data_text	= $data_text."<img class='friends-image img-circle' src='".$like_user['photo']."'/>";
			$data_text	= $data_text."</div>";
		}
		$data_text		= $data_text."</div>";
		$date['text']		= $data_text;
		$asset			= Array();
		$asset['media']		= $checkin['photo'][0]['url'];
		$asset['credit']	= "";
		$asset['caption']	= "";
		$date['asset']		= $asset;
		$dates[$i]		= $date;
		$i++;
	}
	$out['date']	= $dates;
	$outer['timeline'] = $out;

	file_put_contents("Logs/data.json", json_encode($outer));
	#writeFile(json_encode($outer));	
	#echo "File is done";
	header("Location: data.html");
	
?>
