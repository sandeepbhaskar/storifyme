<?php

 class FourSquare{
	private $foursquare_api;

	public function __construct(){
		$this->foursquare_api 	= "https://api.foursquare.com/v2/";
	}

	public function get_access_token($code){
	    require_once('config.php');
	    $url = FOURSQUARE_OAUTH_URL.'?client_id='.CLIENT_ID.'&client_secret='.CLIENT_SECRET.'&grant_type=authorization_code&redirect_uri='.CALLBACK_URL.'&code='.$code;
	    $fsq_data = json_decode(file_get_contents($url));
	    return $fsq_data->access_token;
	}


	public function get_checkins($access_token){
	    $fsq_data	= json_decode(file_get_contents($this->foursquare_api."users/self/checkins?limit=100&oauth_token=".$access_token));
	    $checkins	= $fsq_data->response->checkins->items;
	    $checkin_data 	= Array();
	    $checkin_count	= 0;
	    foreach($checkins as $checkin){
		$like_data 	= Array();
		$with_data	= Array();
		$photo_data	= Array();
		$like_count	= 0;
		$with_count	= 0;
		$photo_cnt	= 0;
		//Venue
		$venue_data	= Array(
			'id'		=> $checkin->venue->id,
			'name'		=> $checkin->venue->name,
			'category'	=> $checkin->venue->categories[0]->name
		);
		//Like
		foreach($checkin->likes->groups[0]->items as $like){
			$like_data[$like_count]['id'] 	= $like->id;
			$like_data[$like_count]['name']	= $like->firstName.' '.$like->lastName;
			$like_data[$like_count]['photo']= $like->photo;
			$like_count++;
		}
		//With
		foreach($checkin->entities as $entity){
			$with_data[$with_count]['id']	= $entity->id;
			$user_details			= $this->get_user($access_token, $entity->id);
			$with_data[$with_count]['name']	= $user_details['name'];
			$with_data[$with_count]['photo']= $user_details['photo'];
			$with_count++; 
		}
		//Photos
		foreach($checkin->photos->items as $photo){
			$photo_data[$photo_cnt]['url']	= $photo->url;
			$photo_cnt++;
		}
	     	$_data	= Array(
			'id'		=> $checkin->id,
			'time'		=> $checkin->createdAt,
			'shout'		=> $checkin->shout,
			'with'		=> $with_data,
			'venue'		=> $venue_data,
			'like'		=> $like_data,
			'photo'		=> $photo_data
		);
		$checkin_data[$checkin_count] = $_data;
		$checkin_count++;
	    }
	    return $checkin_data;
	}

	public function get_user($access_token, $user_id){
            $fsq_data 	= json_decode(file_get_contents($this->foursquare_api."users/".$user_id."?oauth_token=".$access_token));
	    $user	= $fsq_data->response->user;
	    $_data 	= Array(
		'fq_id'	=> $user->id,
		'name'	=> $user->firstName." ".$user->lastName,
		'photo'	=> $user->photo,
		'city'	=> $user->homeCity,
		'email'	=> $user->contact->email,
		'fcount'=> $user->friends->count,
		'ccount'=> $user->checkins->count,
		'at'	=> $access_token
	    );
	    return $_data;	
	}
 }
?>
