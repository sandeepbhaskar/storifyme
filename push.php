<?php
	include  'storify_db.php';
	$checkin 	= $_POST["checkin"];
	$secret 	= $_POST["secret"];

	$db = new StorifyDb();
	//$db->push($checkin, $secret);
	
	$checkin = json_decode('{"id":"509bfce0e4b0da363830550c","createdAt":1352400096,"type":"checkin","private":true,"visibility":"private","timeZone":"Asia/Kolkata","timeZoneOffset":330,"user":{"id":"3788734","firstName":"Sandeep","lastName":"Bhaskar","relationship":"self","photo":"https://is1.4sqi.net/userpix_thumbs/5KFUXMTBEKBWWU22.jpg","tips":{"count":7},"lists":{"groups":[{"type":"created","count":2,"items":[]}]},"gender":"male","homeCity":"Bengaluru, India","bio":"","contact":{"email":"sandeep.bhaskar19@gmail.com","twitter":"sandeepbhaskar","facebook":"716772976"}},"venue":{"id":"4fa903b3e4b022fdec40f063","name":"Ideophone","contact":{},"location":{"lat":12.96582904537425,"lng":77.64762514972156,"country":"India","cc":"IN"},"categories":[{"id":"4bf58dd8d48988d125941735","name":"Tech Startup","pluralName":"Tech Startups","shortName":"Tech Startup","icon":"https://foursquare.com/img/categories/shops/technology.png","parents":["Professional & Other Places","Offices"],"primary":true}],"verified":false,"stats":{"checkinsCount":146,"usersCount":6,"tipCount":1},"likes":{"count":0,"groups":[]},"beenHere":{"count":0},"specials":[]}}');
	
	$checkin = json_decode('{
    "id": "509a08dfe4b07f27238bc71c",
    "createdAt": 1352272095,
    "type": "checkin",
    "isMayor": true,
    "timeZone": "Asia/Kolkata",
    "timeZoneOffset": 330,
    "user": {
        "id": "32228023",
        "firstName": "Swapnil",
        "lastName": "Mishra",
        "relationship": "self",
        "photo": "https://is1.4sqi.net/userpix_thumbs/2D00OOEBQPDTQBYS.jpg",
        "tips": {
            "count": 3
        },
        "lists": {
            "groups": [
                {
                    "type": "created",
                    "count": 1,
                    "items": []
                }
            ]
        },
        "gender": "male",
        "homeCity": "Karnataka",
        "bio": "",
        "contact": {
            "email": "swapmis1084@gmail.com",
            "twitter": "swapnil_mishra",
            "facebook": "589074329"
        }
    },
    "venue": {
        "id": "4c230b3411de20a103d985ce",
        "name": "Northern Route",
        "contact": {},
        "location": {
            "address": "St Marks Road",
            "lat": 12.970324825187628,
            "lng": 77.60064969089264,
            "city": "Bengaluru",
            "state": "Karnataka",
            "country": "India",
            "cc": "IN"
        },
        "categories": [
            {
                "id": "4bf58dd8d48988d10f941735",
                "name": "Indian Restaurant",
                "pluralName": "Indian Restaurants",
                "shortName": "Indian",
                "icon": "https://foursquare.com/img/categories/food/indian.png",
                "parents": [
                    "Food"
                ],
                "primary": true
            }
        ],
        "verified": false,
        "stats": {
            "checkinsCount": 63,
            "usersCount": 20,
            "tipCount": 2
        },
        "likes": {
            "count": 0,
            "groups": []
        },
        "beenHere": {
            "count": 0
        },
        "specials": []
    }
}');
	$venue_data	= Array(
	    'id'	=> $checkin->venue->id,
	    'name'	=> $checkin->venue->name,
	    'category'	=> $checkin->categories[0]->name
	);

	$like_count =  0;
	//Like
	foreach($checkin->likes->groups[0]->items as $like){
		$like_data[$like_count]['id']   = $like->id;
		$like_data[$like_count]['name'] = $like->firstName.' '.$like->lastName;
		$like_data[$like_count]['photo']= $like->photo;
		$like_count++;
	}
	#With
	$with_count = 0;
	foreach($checkin->entities as $entity){
		$with_data[$with_count]['id']   = $entity->id;
		$with_data[$with_count]['name'] = $user_details['name'];
		$with_data[$with_count]['photo']= $user_details['photo'];
                $with_count++;
    	}
        #Photos
	$photo_cnt = 0;
        foreach($checkin->photos->items as $photo){
		$photo_data[$photo_cnt]['url']  = $photo->url;
                $photo_cnt++;
        }


	$checkin_data	= Array(
	    'id'	=> $checkin->id,
	    'time'	=> $checkin->createdAt,
	    'shout'	=> $checkin->shout,
	    'with'	=> $with_data,
	    'venue'	=> $venue_data,
	    'like'	=> $like_data,
	    'photo'	=> $photo_data
	);
	
	echo json_encode($checkin_data);

//	echo json_encode($fq->format_checkins(json_decode($checkin)));

	#$db->push($checkin, $secret);
?>
