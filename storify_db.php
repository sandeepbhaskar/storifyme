<?php

class StorifyDb{
	
	public function push($checkin, $secret){
		return $this->db_insert("INSERT INTO `Log`(checkin, secret) VALUES('".$checkin."','".$secret."') ");
	}

	public function create_user($data){
		return $this->db_insert("INSERT INTO `User` (`user_fq_id`, `user_name`, `user_email`,`user_photo`, `user_access_token`, `user_home_city`, `user_status`) VALUES ('".$data['fq_id']."', '".$data['name']."', '".$data['email']."','".$data['photo']."', '".$data['at']."', '".$data['city']."', 1);");
	}

	private function create_dummy_user($user_fq_id, $user_name, $user_photo){
		return $this->db_insert("INSERT INTO `User`(`user_fq_id`,`user_name`,`user_photo`) VALUES('".$user_fq_id."','".$user_name."','".$user_photo."')");
	}

	private function create_venue($data){
		return $this->db_insert("INSERT INTO `Venue`(`venue_fq_id`,`venue_name`,`venue_category`) VALUES('".$data['id']."','".$data['name']."','".$data['category']."') ");
	}

	private function create_meta($checkin_fq_id, $user, $relation_type){
		$this->create_dummy_user($user['id'], $user['name'], $user['photo']);
		return $this->db_insert("INSERT INTO `MetaUser`(`meta_checkin_fq_id`,`meta_user_fq_id`,`meta_relation_type`)  VALUES('".$checkin_fq_id."','".$user['id']."','".$relation_type."')");
	}

	public function create_checkin($data, $user_fq_id){
		$this->create_venue($data['venue']);
		$this->db_insert("INSERT INTO `Checkin`(`checkin_fq_id`,`checkin_user_fq_id`,`checkin_venue_fq_id`,`checkin_time`,`checkin_shout`,`checkin_photo`) VALUES('".$data['id']."','".$user_fq_id."','".$data['venue']['id']."','".$data['time']."','".$data['shout']."','".$data['photo'][0]['url']."')");
		foreach($data['with'] as $with_user){
			$this->create_meta($data['id'], $with_user, 'with');
		}

		foreach($data['like'] as $like_user){
			$this->create_meta($data['id'], $like_user, 'like');
		}
		return true;
	}

	private function db_query($query_string){
		try{
			$result_set = Array();
			$i=0;
			if($this->db_init()){
				if($result = mysql_query($query_string)){
					while($row = mysql_fetch_array($result)){
						$result_set[$i]	= $row;
						$i++;
					}
					return $result_set;
				}
			}
			return Array();
		}
		catch(Exception $e){
			return Array();
		}
	}

	private function db_init(){
		try{
			$iniP		= parse_ini_file('s.ini');
			$connect	= mysql_connect($iniP['dbPath'], $iniP['dbUser'], 'story123$');
			if(!$connect){
				return false;
			}
			mysql_select_db($iniP['dbName']);
			return true;
		}
		catch(Exception $e){
			return false;
		}
    }

    private function db_insert($insert_string){
		try{
			if($this->db_init()){
				$result = mysql_query($insert_string);
				return true;
			}
			return false;
		}
		catch(Exception $e){
			return false;
		}
	}

	private function db_insert_id($insert_string){
		try{
			if($this->db_init()){
				$result = mysql_query($insert_string);
				return mysql_insert_id();
			}
			return 0;
		}
		catch(Exception $e){
			return 0;
		}
	}
}
?>

